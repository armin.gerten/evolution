﻿using System;

namespace Evolution.Simulation
{
    public struct Coordinate
    {
        public Coordinate(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public static Coordinate Default => new Coordinate(-1, -1);

        public int X { get; set; }
        public int Y { get; set; }

        public override bool Equals(object obj)
        {
            if (!(obj is Coordinate))
            {
                return false;
            }

            var coordinate = (Coordinate)obj;
            return this.X == coordinate.X &&
                   this.Y == coordinate.Y;
        }

        public override int GetHashCode() => HashCode.Combine(this.X, this.Y);

        public static bool operator ==(Coordinate lhs, Coordinate rhs)
            => lhs.X == rhs.X && lhs.Y == rhs.Y;

        public static bool operator !=(Coordinate lhs, Coordinate rhs)
            => !(lhs == rhs);
    }
}
