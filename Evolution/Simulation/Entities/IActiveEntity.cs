﻿namespace Evolution.Simulation.Entities
{
    public interface IActiveEntity : IEntity
    {
        int Initiative { get; }

        void Activate();
    }
}
