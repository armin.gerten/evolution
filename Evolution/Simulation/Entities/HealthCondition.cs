﻿namespace Evolution.Simulation.Entities
{
    public enum HealthCondition
    {
        Neutral,
        Healthy,
        Dying
    }
}
