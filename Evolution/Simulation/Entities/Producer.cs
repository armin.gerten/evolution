﻿using System;

namespace Evolution.Simulation.Entities
{
    public class Producer : IActiveEntity
    {
        private readonly Map map;

        public Producer(Map map, Coordinate targetPosition, int initiative)
        {
            this.map = map;
            this.map.Spawn(this, targetPosition);

            this.Initiative = initiative;
            this.Energy = 0;
        }

        public int Initiative { get; private set; }

        public int Energy { get; private set; }

        public int ExtraEnergyChancePercent => 5;

        public int ReproductionThreshold = 20;

        public Coordinate Position { get; set; }

        public void Activate()
        {
            this.Energy++;
            if (new Random().Next(1, 101) <= this.ExtraEnergyChancePercent)
            {
                this.Energy++;
            }

            if (this.Energy >= this.ReproductionThreshold)
            {
                Fruit fruit = new Fruit();
                this.map.Spawn(fruit, this.Position, 1);
                this.Energy -= this.ReproductionThreshold;
            }
        }
    }
}
