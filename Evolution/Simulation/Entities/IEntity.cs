﻿namespace Evolution.Simulation.Entities
{
    public interface IEntity
    {
        Coordinate Position { get; set; }
    }
}
