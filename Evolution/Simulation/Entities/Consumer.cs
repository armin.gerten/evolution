﻿namespace Evolution.Simulation.Entities
{
    public class Consumer : IActiveEntity
    {
        private readonly Map map;

        public int Speed => 1;

        public int Sociality => 1;

        public int Sight => 10;

        public int BreedingEnergy => 200;

        public int Energy { get; private set; }

        public int Initiative { get; private set; }

        public HealthCondition Condition
        {
            get
            {
                if (this.Energy < 30)
                {
                    return HealthCondition.Dying;
                }
                else if (this.Energy > 150)
                {
                    return HealthCondition.Healthy;
                }
                else
                {
                    return HealthCondition.Neutral;
                }
            }
        }

        public Coordinate Position { get; set; }

        public Consumer(Map map, Coordinate targetPosition, int initiative, int energy)
        {
            this.map = map;
            this.map.Spawn(this, targetPosition);

            this.Initiative = initiative;
            this.Energy = energy;
        }

        public void Activate()
        {
            Coordinate focus = this.DetermineFocus();

            this.Move(focus);

            // De-spawn if out of energy
            if (this.Energy <= 0)
            {
                this.map.Despawn(this);
            }

            // Spawn descendants
            if (this.Energy >= this.BreedingEnergy)
            {
                // Descendant has same properties and inherits half of the parent's energy
                new Consumer(this.map, this.Position, this.Initiative, this.Energy / 2);

                // Spawning descendants takes half of the energy
                this.Energy = this.Energy / 2;
            }
        }

        private Coordinate DetermineFocus()
        {
            // Target closest adible entity
            // If no edible entity exists, no entity is focused
            // If multiple entities have the same distance, target slowest (highest initiative)
            // If multiple entities have the same distance and initiative, target random

            Coordinate focus = this.map.FindClosestEdible(this.Position, this.Sight);
            if (focus != Coordinate.Default)
            {
                return focus;
            }
            return this.Position;
        }

        private void Move(Coordinate targetPosition)
        {
            // If target is current position, no focus => move to random position
            if (targetPosition == this.Position)
            {
                this.Energy += this.map.MoveToEmpty(this, this.Speed);
                return;
            }

            // Modify energy based on movement / eating
            this.Energy += this.map.MoveAndEat(this, targetPosition);

            // Move to entity using available speed
            // Moving reduces energy by distance moved
            // If entering cell occupied with adible entity, moving action stops immediately
            // Eat
        }
    }
}
