﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using Evolution.Metrics;
using Evolution.Simulation.Entities;

namespace Evolution.Simulation
{
    public class Map
    {
        // TODO: read http://www.cokeandcode.com/main/tutorials/path-finding/

        private const int MinimapScaleFactor = 19;
        private const int GridThickness = 1;
        private const int XBound = 50;
        private const int YBound = 50;

        private readonly IEntity[,] cells = new IEntity[XBound, YBound];
        private readonly List<Consumer> consumers = new List<Consumer>();
        private readonly List<Producer> producers = new List<Producer>();
        private readonly List<Fruit> fruits = new List<Fruit>();

        private readonly MapMetrics metrics;
        private readonly Stopwatch stopwatch;

        public Map(MapMetrics metrics)
        {
            this.metrics = metrics;

            this.stopwatch = new Stopwatch();
        }

        public IReadOnlyCollection<IActiveEntity> ActiveEntities
        {
            get
            {
                List<IActiveEntity> entities = new List<IActiveEntity>(this.consumers.Count + this.producers.Count);
                entities.AddRange(this.consumers);
                entities.AddRange(this.producers);
                return entities.AsReadOnly();
            }
        }

        public void Spawn(IEntity entity, Coordinate targetPosition, int maxRadius = 3)
        {
            Coordinate correctedPosition = new Coordinate(targetPosition.X % XBound, targetPosition.Y % YBound);
            Coordinate closestEmptyCell = this.DetermineEmptyCell(correctedPosition, maxRadius);

            if (closestEmptyCell != Coordinate.Default)
            {
                this.cells[closestEmptyCell.X, closestEmptyCell.Y] = entity;
                switch (entity)
                {
                    case Consumer consumer:
                        this.consumers.Add(consumer);
                        this.metrics.ConsumerCount++;
                        break;
                    case Producer producer:
                        this.producers.Add(producer);
                        this.metrics.ProducerCount++;
                        break;
                    case Fruit fruit:
                        this.fruits.Add(fruit);
                        this.metrics.FruitCount++;
                        break;
                    default:
                        break;
                }
            }

            entity.Position = closestEmptyCell;
        }

        public void Despawn(IEntity entity)
        {
            this.cells[entity.Position.X, entity.Position.Y] = null;
            switch (entity)
            {
                case Consumer consumer:
                    this.consumers.Remove(consumer);
                    this.metrics.ConsumerCount--;
                    break;
                case Producer producer:
                    this.producers.Remove(producer);
                    this.metrics.ProducerCount--;
                    break;
                case Fruit fruit:
                    this.fruits.Remove(fruit);
                    this.metrics.FruitCount--;
                    break;
                default:
                    break;
            }
        }

        public Coordinate FindClosestEdible(Coordinate position, int viewDistance)
        {
            Coordinate closestEntity = Coordinate.Default;
            int closestDistance = XBound + YBound;
            foreach (Fruit fruit in this.fruits)
            {
                int distance = Math.Abs(position.X - fruit.Position.X) + Math.Abs(position.Y - fruit.Position.Y);
                if (distance < closestDistance && distance <= viewDistance)
                {
                    closestEntity = fruit.Position;
                    closestDistance = distance;
                }
            }
            return closestEntity;
        }

        public int MoveAndEat(Consumer consumer, Coordinate targetPosition)
        {
            Fruit targetedFruit = this.cells[targetPosition.X, targetPosition.Y] as Fruit;
            int remainingMoveDistance = consumer.Speed;
            int traveledDistance = 0;

            // Move towards targets until speed limit has reached or moving is no longer possible
            while (remainingMoveDistance > 0)
            {
                if (this.TryMoveOne(consumer, targetPosition))
                {
                    remainingMoveDistance--;
                    traveledDistance++;
                }
                else
                {
                    remainingMoveDistance = 0;
                }
            }

            // If current position is target position and fruit, eat it
            if (consumer.Position == targetPosition && targetedFruit != null)
            {
                this.fruits.Remove(targetedFruit);
                this.metrics.FruitCount--;
                return targetedFruit.NutritiveValue - traveledDistance;
            }

            return -1 * traveledDistance;
        }

        private bool TryMoveOne(IEntity entity, Coordinate targetPosition)
        {
            Coordinate origin = entity.Position;
            bool moved = false;

            // Try moving one step along x axis
            if (Math.Abs(targetPosition.X - entity.Position.X) > Math.Abs(targetPosition.Y - entity.Position.Y))
            {
                // Move right
                if (targetPosition.X > entity.Position.X)
                {
                    if (!(this.cells[(entity.Position.X + 1) % XBound, entity.Position.Y] is IActiveEntity))
                    {
                        entity.Position = new Coordinate((entity.Position.X + 1) % XBound, entity.Position.Y);
                        moved = true;
                    }
                }
                // Move left
                else
                {
                    if (!(this.cells[(entity.Position.X - 1 + XBound) % XBound, entity.Position.Y] is IActiveEntity))
                    {
                        entity.Position = new Coordinate((entity.Position.X - 1 + XBound) % XBound, entity.Position.Y);
                        moved = true;
                    }
                }
            }

            // Otherwise try moving one step along y axis
            if (!moved)
            {
                // Move down
                if (targetPosition.Y > entity.Position.Y)
                {
                    if (!(this.cells[entity.Position.X, (entity.Position.Y + 1) % YBound] is IActiveEntity))
                    {
                        entity.Position = new Coordinate(entity.Position.X, (entity.Position.Y + 1) % YBound);
                        moved = true;
                    }
                }
                // Move up
                else
                {
                    if (!(this.cells[entity.Position.X, (entity.Position.Y - 1 + YBound) % YBound] is IActiveEntity))
                    {
                        entity.Position = new Coordinate(entity.Position.X, (entity.Position.Y - 1 + YBound) % YBound);
                        moved = true;
                    }
                }
            }

            if (moved)
            {
                this.cells[origin.X, origin.Y] = null;
                this.cells[entity.Position.X, entity.Position.Y] = entity;
            }

            return moved;
        }

        public int MoveToEmpty(IActiveEntity entity, int distance)
        {
            // TODO: This algorithm is a little whacky...

            Coordinate targetPosition = entity.Position;
            for (int steps = 1; steps <= distance; steps++)
            {
                targetPosition = this.DetermineEmptyNeighborCoordinate(targetPosition);
            }

            if (targetPosition != entity.Position)
            {
                this.cells[entity.Position.X, entity.Position.Y] = null;
                this.cells[targetPosition.X, targetPosition.Y] = entity;
            }

            entity.Position = targetPosition;

            return -1 * distance;
        }

        private Coordinate DetermineEmptyNeighborCoordinate(Coordinate position)
        {
            List<Coordinate> emptyNeighbors = new List<Coordinate>(4);

            if (this.cells[(position.X - 1 + XBound) % XBound, position.Y] == null)
                emptyNeighbors.Add(new Coordinate((position.X - 1 + XBound) % XBound, position.Y));

            if (this.cells[(position.X + 1) % XBound, position.Y] == null)
                emptyNeighbors.Add(new Coordinate((position.X + 1) % XBound, position.Y));

            if (this.cells[position.X, (position.Y - 1 + YBound) % YBound] == null)
                emptyNeighbors.Add(new Coordinate(position.X, (position.Y - 1 + YBound) % YBound));

            if (this.cells[position.X, (position.Y + 1) % YBound] == null)
                emptyNeighbors.Add(new Coordinate(position.X, (position.Y + 1) % YBound));

            if (emptyNeighbors.Count == 0)
            {
                return position;
            }

            return emptyNeighbors[new Random().Next(0, emptyNeighbors.Count)];
        }

        /// <summary>
        /// Determines the next empty cell within the specified max radius, starting from the center.
        /// </summary>
        /// <param name="position">Starting position.</param>
        /// <param name="maxRadius">The maximum radius to search for empty cells.</param>
        /// <returns>The closest empty cell or <see cref="Coordinate.Default" />.</returns>
        private Coordinate DetermineEmptyCell(Coordinate position, int maxRadius)
        {
            if (this.cells[position.X, position.Y] == null)
            {
                return position;
            }

            for (int radius = 1; radius <= maxRadius; radius++)
            {
                for (int x = position.X - radius; x <= position.X + radius; x++)
                {
                    for (int y = position.Y - radius; y <= position.Y + radius; y++)
                    {
                        if (this.cells[(x + XBound) % XBound, (y + YBound) % YBound] == null)
                        {
                            return new Coordinate((x + XBound) % XBound, (y + YBound) % YBound);
                        }
                    }
                }
            }
            return Coordinate.Default;
        }

        public Image GetMinimapGridImage()
        {
            Image image = new Bitmap(XBound * MinimapScaleFactor + GridThickness, YBound * MinimapScaleFactor + GridThickness);
            using (Graphics drawing = Graphics.FromImage(image))
            using (Pen gridPen = new Pen(Color.Black))
            {
                drawing.Clear(Color.White);

                // Draw vertical grid
                for (int x = 0; x <= XBound * MinimapScaleFactor; x += MinimapScaleFactor)
                    drawing.DrawLine(gridPen, x, 0, x, image.Size.Height);

                // Draw horizontal grid
                for (int y = 0; y <= YBound * MinimapScaleFactor; y += MinimapScaleFactor)
                    drawing.DrawLine(gridPen, 0, y, image.Size.Width, y);

                drawing.Save();
            }

            return image;
        }

        public Image GetMinimapOverlayImage()
        {
            this.stopwatch.Restart();

            int consumerPadding = MinimapScaleFactor / 8;
            int producerPadding = MinimapScaleFactor / 10;
            int fruitPadding = MinimapScaleFactor / 5;

            Bitmap image = new Bitmap(XBound * MinimapScaleFactor + GridThickness, YBound * MinimapScaleFactor + GridThickness, PixelFormat.Format32bppPArgb);
            using (Graphics drawing = Graphics.FromImage(image))
            using (Brush healthyConsumerBrush = new SolidBrush(Color.LightGreen))
            using (Brush neutralconsumerBrush = new SolidBrush(Color.CornflowerBlue))
            using (Brush dyingConsumerBrush = new SolidBrush(Color.DarkRed))
            using (Brush producerBrush = new SolidBrush(Color.BurlyWood))
            using (Brush fruitBrush = new SolidBrush(Color.Red))
            {
                for (int x = 0; x < XBound; x++)
                    for (int y = 0; y < YBound; y++)
                    {
                        if (this.cells[x, y] != null)
                        {
                            switch (this.cells[x, y])
                            {
                                case Consumer consumer:
                                    HealthCondition condition = consumer.Condition;
                                    Brush brush = condition == HealthCondition.Healthy ? healthyConsumerBrush : condition == HealthCondition.Dying ? dyingConsumerBrush : neutralconsumerBrush;
                                    // drawing.FillEllipse(brush, x * MinimapScaleFactor + 2, y * MinimapScaleFactor + 2, MinimapScaleFactor - 3, MinimapScaleFactor - 3);
                                    drawing.FillEllipse(brush,
                                        x * MinimapScaleFactor + GridThickness + consumerPadding,
                                        y * MinimapScaleFactor + GridThickness + consumerPadding,
                                        MinimapScaleFactor - GridThickness - 2 * consumerPadding,
                                        MinimapScaleFactor - GridThickness - 2 * consumerPadding);
                                    break;
                                case Producer _:
                                    //drawing.FillRectangle(producerBrush, x * MinimapScaleFactor + 2, y * MinimapScaleFactor + 2, MinimapScaleFactor - 3, MinimapScaleFactor - 3);
                                    drawing.FillRectangle(producerBrush,
                                        x * MinimapScaleFactor + GridThickness + producerPadding,
                                        y * MinimapScaleFactor + GridThickness + producerPadding,
                                        MinimapScaleFactor - GridThickness - 2 * producerPadding,
                                        MinimapScaleFactor - GridThickness - 2 * producerPadding);
                                    break;
                                case Fruit _:
                                    //drawing.FillEllipse(fruitBrush, x * MinimapScaleFactor + 4, y * MinimapScaleFactor + 4, MinimapScaleFactor - 7, MinimapScaleFactor - 7);
                                    drawing.FillEllipse(fruitBrush,
                                        x * MinimapScaleFactor + GridThickness + fruitPadding,
                                        y * MinimapScaleFactor + GridThickness + fruitPadding,
                                        MinimapScaleFactor - GridThickness - 2 * fruitPadding,
                                        MinimapScaleFactor - GridThickness - 2 * fruitPadding);
                                    break;
                            }
                        }
                    }

                drawing.Save();
            }

            this.stopwatch.Stop();
            this.metrics.MinimapDrawDuration.Put(this.stopwatch.ElapsedMilliseconds);

            return image;
        }
    }
}
