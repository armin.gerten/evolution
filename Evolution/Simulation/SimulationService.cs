﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Timers;
using Evolution.Metrics;
using Evolution.Simulation.Entities;

namespace Evolution.Simulation
{
    public class SimulationService
    {
        private readonly Timer simulationTimer;
        private readonly SimulationMetrics metrics;
        private readonly Stopwatch stopwatch;

        public SimulationService(Map map, SimulationMetrics metrics)
        {
            this.Map = map;
            this.metrics = metrics;

            this.CreateRandomSimulation();

            this.stopwatch = new Stopwatch();

            this.simulationTimer = new Timer(200);
            this.simulationTimer.Elapsed += this.OnSimulationTimerTimerElapsed;
        }

        public Map Map { get; }

        public void Start()
            => this.simulationTimer.Start();

        private void CreateRandomSimulation()
        {
            Random random = new Random();

            for (int c = 0; c < 20; c++)
            {
                new Consumer(this.Map, new Coordinate(random.Next(0, 100), random.Next(0, 100)), c, 100);
            }

            for (int p = 0; p < 30; p++)
            {
                new Producer(this.Map, new Coordinate(random.Next(0, 100), random.Next(0, 100)), 200 + p);
            }
        }


        private void OnSimulationTimerTimerElapsed(object sender, ElapsedEventArgs e)
        {
            this.stopwatch.Restart();

            foreach (IActiveEntity entity in this.Map.ActiveEntities.OrderBy(c => c.Initiative))
            {
                entity.Activate();
            }

            this.stopwatch.Stop();
            this.metrics.SimulationDuration.Put(this.stopwatch.ElapsedMilliseconds);
        }
    }
}
