﻿namespace Evolution.Metrics
{
    public class SimulationMetrics
    {
        public HistoricGauge SimulationDuration { get; } = new HistoricGauge(10);
    }
}
