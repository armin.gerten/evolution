﻿namespace Evolution.Metrics
{
    public class MapMetrics
    {
        public HistoricGauge MinimapDrawDuration { get; } = new HistoricGauge(10);

        public int ConsumerCount { get; set; }

        public int ProducerCount { get; set; }

        public int FruitCount { get; set; }
    }
}
