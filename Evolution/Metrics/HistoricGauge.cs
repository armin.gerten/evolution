﻿using System.Linq;

namespace Evolution.Metrics
{
    public class HistoricGauge
    {
        private readonly SlidingBuffer<long> buffer;
        private long lastValue;

        public HistoricGauge(int bufferLength)
            => this.buffer = new SlidingBuffer<long>(bufferLength);

        public long Get()
            => this.lastValue;

        public void Put(long value)
        {
            this.buffer.Add(value);
            this.lastValue = value;
        }

        public long Average()
            => System.Convert.ToInt64(this.buffer.Average());
    }
}
