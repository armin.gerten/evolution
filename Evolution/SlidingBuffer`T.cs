﻿using System.Collections;
using System.Collections.Generic;

namespace Evolution
{
    public class SlidingBuffer<T> : IEnumerable<T>
    {
        private readonly Queue<T> queue;
        private readonly int maxCount;

        public SlidingBuffer(int maxCount)
        {
            this.maxCount = maxCount;
            this.queue = new Queue<T>(maxCount);
        }

        public void Add(T item)
        {
            if (this.queue.Count == this.maxCount)
                this.queue.Dequeue();
            this.queue.Enqueue(item);
        }

        public IEnumerator<T> GetEnumerator() => this.queue.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
    }
}
