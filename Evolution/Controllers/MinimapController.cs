﻿using System.Drawing;
using System.IO;
using Evolution.Simulation;
using Microsoft.AspNetCore.Mvc;

namespace Evolution.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MinimapController : ControllerBase
    {
        private readonly Map map;

        public MinimapController(Map map)
            => this.map = map;

        [HttpGet("grid")]
        public FileContentResult GetGrid()
        {
            using (Image image = this.map.GetMinimapGridImage())
            {
                return this.File(this.ImageToByteArray(image), "image/png");
            }
        }

        [HttpGet("overlay")]
        public FileContentResult GetOverlay()
        {
            using (Image image = this.map.GetMinimapOverlayImage())
            {
                return this.File(this.ImageToByteArray(image), "image/png");
            }
        }

        public byte[] ImageToByteArray(Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            return ms.ToArray();
        }
    }
}
