﻿using Evolution.Metrics;
using Microsoft.AspNetCore.Mvc;

namespace Evolution.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MetricsController : ControllerBase
    {
        private readonly MapMetrics mapMetrics;
        private readonly SimulationMetrics simulationMetrics;

        public MetricsController(MapMetrics mapMetrics, SimulationMetrics simulationMetrics)
        {
            this.mapMetrics = mapMetrics;
            this.simulationMetrics = simulationMetrics;
        }

        [HttpGet]
        public ActionResult<Metrics> Get()
            => new Metrics
            {
                AverageMinimapDrawDuration = this.mapMetrics.MinimapDrawDuration.Average(),
                AverageSimulationDuration = this.simulationMetrics.SimulationDuration.Average(),
                ConsumerCount = this.mapMetrics.ConsumerCount,
                ProducerCount = this.mapMetrics.ProducerCount,
                FruitCount = this.mapMetrics.FruitCount
            };
    }
}
