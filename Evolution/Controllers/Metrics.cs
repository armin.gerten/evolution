﻿namespace Evolution.Controllers
{
    public class Metrics
    {
        public long AverageMinimapDrawDuration;

        public long AverageSimulationDuration;

        public long ConsumerCount;

        public long ProducerCount;

        public long FruitCount;
    }
}
